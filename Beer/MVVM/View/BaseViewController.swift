    //
//  BaseViewController.swift
//  Beer
//
//  Created by Ygor Pessoa on 7/25/17.
//  Copyright © 2017 Ygor Pessoa. All rights reserved.
//

import UIKit

    
    
class BaseViewController: UIViewController {
    
    
    override func viewDidLoad() {
        
    }
    
    public enum NavigationType {
        case Default, White
    }
    
    func showConfirmAlert(_ message: String, _ action: ((UIAlertAction) -> Void)?){
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: action))
        self.present(alert, animated: true, completion: nil)
    }
    
    func customizeNavigation(type: NavigationType = .Default){
        
        
        
        switch type {
        case .White:
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
            navigationController?.navigationBar.isTranslucent = false
            navigationController?.navigationBar.barTintColor = .white
            navigationController?.navigationBar.tintColor = .black
            navigationController?.navigationBar.backgroundColor = .white
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.black]
        default:
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
            navigationController?.navigationBar.isTranslucent = false
            navigationController?.navigationBar.barTintColor = UIColor.black
            navigationController?.navigationBar.tintColor = .white
            navigationController?.navigationBar.backgroundColor = UIColor.black
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        }
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        
    }
    
}
