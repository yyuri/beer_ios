//
//  ServiceAPI.swift
//  Beer
//
//  Created by Ygor Pessoa on 7/25/17.
//  Copyright © 2017 Ygor Pessoa. All rights reserved.
//


import Alamofire


public struct Response<T> {
    var data: T?
    let result: Result
}

public enum Result {
    case success
    case error(message: String)
}

class ServiceAPI {
    
    static let HOST = "https://api.punkapi.com/v2";
    
    static let shared: ServiceAPI = ServiceAPI()
    private init() {}
    
    func beerList(callback: @escaping (Response<[Beer]>) -> Void) {
        let urlString = String(format: "%@/beers/random", ServiceAPI.HOST)
        guard let url = URL(string: urlString) else {
            callback(Response<[Beer]>(data: nil, result: Result.error(message: "Ocorreu um erro")))
            return
        }
        
        Alamofire.request(url, method: .get).responseJSON{ response in
            switch response.result {
            case .success:
                print(response)
                if let data = response.result.value as? Array<[String: Any]> {
                    if let statusCode = response.response?.statusCode {
                        if statusCode == 200 {
                            var array: [Beer] = []
                            _ = data.map {
                                if let beer = Beer(JSON: $0) {
                                    array.append(beer)
                                }
                                
                            }
                            callback(Response<[Beer]>(data: array, result: Result.success))
                            return
                        }
                    }
                    callback(Response<[Beer]>(data: nil, result:Result.error(message: "Ocorreu um erro")))
                } else {
                    callback(Response<[Beer]>(data: nil, result:Result.error(message: "Ocorreu um erro")))
                }
                break
            case .failure(let error):
                callback(Response<[Beer]>(data: nil, result:Result.error(message: error.localizedDescription)))
                break
                
            }
        }
        
    }
}

