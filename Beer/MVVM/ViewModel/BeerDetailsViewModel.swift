//
//  BeerDetailsViewModel.swift
//  Beer
//
//  Created by Ygor Pessoa on 7/25/17.
//  Copyright © 2017 Ygor Pessoa. All rights reserved.
//

import Foundation


class BeerDetailsViewModel {
    
    var items: [(title: String, value: String?)] = []
    
    func fetchItems(beer: Beer){
        items.removeAll()
        items.append((title: "Tagline", value: beer.tagline))
        items.append((title: "Teor alcoólico", value: beer.abv?.description))
        items.append((title: "Escala de amargor", value: beer.ibu?.description))
        items.append((title: "Descrição", value: beer.description))
        
    }
    
}
