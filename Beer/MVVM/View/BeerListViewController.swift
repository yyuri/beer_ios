//
//  BeerListViewController.swift
//  Beer
//
//  Created by Ygor Pessoa on 7/25/17.
//  Copyright © 2017 Ygor Pessoa. All rights reserved.
//

import UIKit
import Kingfisher

class BeerListViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.register(UINib(nibName: BeerCell.nibName, bundle: nil), forCellReuseIdentifier: BeerCell.identifier)
        }
    }
    
    let viewModel = BeerListViewModel()
    
    override func viewDidLoad() {
        title = "Beers"
        viewModel.getBeerList { (result) in
            if result.success {
                self.tableView.reloadData()
            } else {
                self.showConfirmAlert(result.errorMessage ?? "", nil)
            }
        }
        setupTableHeaderView()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        setupTableHeaderView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        customizeNavigation()
    }
    
    fileprivate func setupTableHeaderView(){
        var frame = UIScreen.main.bounds
        if UIDevice.current.orientation.isLandscape {
            frame.size.height = 200
        } else {
            frame.size.height = 250
        }
        
        
        let view = UIImageView(frame: frame)
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.image = UIImage(named: "banner")
        self.tableView.tableHeaderView = view
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsSegue",
            let vc = segue.destination as? BeerDetailsViewController,
            let beer = sender as? Beer {
            vc.beer = beer
        }
    }
}

extension BeerListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.viewModel.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: BeerCell.identifier) as? BeerCell {
            let beer = self.viewModel.list[indexPath.row]
            cell.name.text = beer.name
            if let abv = beer.abv?.description {
                cell.abv.text = String(format: "Teor alcoólico: %@", abv)
            }
            if let imageUrl = beer.imageUrl {
                cell.beerImage.kf.setImage(with: URL(string: imageUrl), placeholder: UIImage(named: "ic_beer_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            return cell
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let beer = self.viewModel.list[indexPath.row]
        self.performSegue(withIdentifier: "detailsSegue", sender: beer)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
