//
//  DetailCell.swift
//  Beer
//
//  Created by Ygor Pessoa on 7/26/17.
//  Copyright © 2017 Ygor Pessoa. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {
    
    static let identifier = "DetailCell"
    static let nibName = "DetailCell"

    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
