//
//  BeerDetailsViewController.swift
//  Beer
//
//  Created by Ygor Pessoa on 7/25/17.
//  Copyright © 2017 Ygor Pessoa. All rights reserved.
//

import UIKit

class BeerDetailsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = 60
            tableView.register(UINib(nibName: DetailCell.nibName, bundle: nil), forCellReuseIdentifier: DetailCell.identifier)
            tableView.separatorStyle = .none
        }
    }
    
    var beer: Beer?
    var viewModel = BeerDetailsViewModel()
    
    override func viewDidLoad() {
        if let name = beer?.name {
            title = name
        }
        setupTableHeaderView()
        if let beer = beer {
            viewModel.fetchItems(beer: beer)
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        customizeNavigation(type: .White)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        setupTableHeaderView()
    }
    
    fileprivate func setupTableHeaderView(){
        var frame = UIScreen.main.bounds
        var h = (frame.size.height - 64) * 0.5
        frame.size.height = h
        
        let view = UIImageView(frame: frame)
        view.contentMode = .scaleAspectFit
        view.clipsToBounds = true
        
        if let image = beer?.imageUrl {
            view.kf.setImage(with: URL(string: image))
            
        }
        self.tableView.tableHeaderView = view
    }
    
    
}

extension BeerDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: DetailCell.identifier) as? DetailCell {
            let item = self.viewModel.items[indexPath.row]
            cell.title.text = item.title
            cell.value.text = item.value
            
            return cell
            
        }
        return UITableViewCell()
    }
    }
