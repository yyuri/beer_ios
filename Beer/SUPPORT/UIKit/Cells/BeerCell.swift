//
//  BeerCell.swift
//  Beer
//
//  Created by Ygor Pessoa on 7/25/17.
//  Copyright © 2017 Ygor Pessoa. All rights reserved.
//

import UIKit

class BeerCell: UITableViewCell {
    
    static let identifier = "BeerCell"
    static let nibName = "BeerCell"

    @IBOutlet weak var beerImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var abv: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
