//
//  Beer.swift
//  Beer
//
//  Created by Ygor Pessoa on 7/25/17.
//  Copyright © 2017 Ygor Pessoa. All rights reserved.
//

import ObjectMapper


class Beer: Mappable {
    var id: Int?
    var name: String?
    var tagline: String?
    var firstBrewed: String?
    var description: String?
    var imageUrl: String?
    var abv: Float?
    var ibu: Int?
    
    init() {}
    
    required init?(map: Map) {}
    func mapping(map: Map) {
        id              <- map["id"]
        name            <- map["name"]
        tagline         <- map["tagline"]
        firstBrewed     <- map["first_brewed"]
        description     <- map["description"]
        imageUrl        <- map["image_url"]
        abv             <- map["abv"]
        ibu             <- map["ibu"]
    }
}
