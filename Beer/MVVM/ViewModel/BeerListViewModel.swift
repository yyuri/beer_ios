//
//  BeerListViewModel.swift
//  Beer
//
//  Created by Ygor Pessoa on 7/25/17.
//  Copyright © 2017 Ygor Pessoa. All rights reserved.
//

import Foundation

class BeerListViewModel {
    
    var list: [Beer] = []
    typealias ResponseClosure = (success: Bool, errorMessage: String?)
    
    func getBeerList(closure: @escaping (_ response: ResponseClosure) -> Void) {
        ServiceAPI.shared.beerList() { (response) in
            switch response.result{
            case .success:
                if let data = response.data {
                    self.list = data
                    closure(ResponseClosure(success: true, errorMessage: nil))
                } else {
                    closure(ResponseClosure(success: false, errorMessage: "Ocorreu um erro"))
                }
                break
            case .error(let message):
                closure(ResponseClosure(success: false, errorMessage: message))
                break
            }
        }
    }
    
}
